import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TvEndpointTests
{
    /*
        The tv_id parameter is supposed to be a valid integer, so let's test that a good error comes back.
        List of api status codes and messages is at https://www.themoviedb.org/documentation/api/status-codes
     */
    @Test
    @DisplayName("Test that GETs /{tv_id} endpoint returns proper messaging with an incorrect input")
    void invalidTvIdInput()
    {
        // This test fails because the actual message is "The resource you requested could not be found." which means
        // it still tried to use "a" for the tv_id even though it isn't an integer, or the message is wrong.
        get("/tv/a").
        then().body("status_message", equalTo("Invalid id: The pre-requisite id is invalid or not found."));
    }

    /*
        Most TMDB API calls allow a language query parameter which returns some translated text in the response.
        The expected response of a bad/unsupported query parameter should be "bad request" or "unprocessable entity"
     */
    @Test
    @DisplayName("Test that the language query string actually adheres to ISO 639-1")
    void invalidTvLanguageQueryString()
    {
        // "ze" is not a valid ISO-639-1 code. For reference: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
        given().param("language", "ze").
        get("/tv/67178").
        // Test fails here because the API doesn't care if the language request parameter is invalid.
        then().body("status_message", equalTo(" Invalid parameters: Your request parameters are incorrect."));
    }

    @Test
    @DisplayName("Test that the default page of a paginated result is #1, and that we can't go lower.")
    void minTvPagination()
    {
        get("/tv/popular").
        then().body("page", equalTo(1));

        // Test page 0
        given().param("page", 0).
        get("/tv/popular").
        then().
        body("errors", hasItem("page must be greater than 0"));

        // Test a negative page
        given().param("page", -1).
        get("/tv/popular").
        then().
        body("errors", hasItem("page must be greater than 0"));
    }

    @Test
    @DisplayName("Test that we can get to the last page of a paginated result, and verify we can't go further.")
    void maxTvPagination()
    {
        // We're using the /popular endpoint because it can return over 1000 pages.
        int totalPages = get("/tv/popular").then().extract().path("total_pages");

        given().param("page", String.valueOf(totalPages)).
        get("/tv/popular").
        then().
            // This fails here because they set their pagination search limit to 1000 even though it can go further than that.
            body("total_pages", equalTo(totalPages)).
            body("results", notNullValue());

        // Test one more page than should be possible
        given().param("page", String.valueOf(totalPages + 1)).
        get("/tv/popular").
        then().
            body(containsString("errors"));
    }

    @Test
    @Disabled("Stub test")
    void minTvRating()
    {
        /*
             TODO: There needs to be a minimum value test on the input allowed for posting a TV rating.
             The listed minimum value is the "number" 0.5 (not listed as decimal or other data type)
         */
    }

    @Test
    @Disabled("Stub test")
    void maxTvRating()
    {
        /*
             TODO: There needs to be a maximum value test on the input allowed for posting a TV rating.
             The listed maximum value is the "number" 10 (not listed as decimal or other data type)
         */
    }

    @Test
    @Disabled("Stub test")
    void deleteTvRating()
    {
        /*
             TODO: Deleting a rating should affect its overall result
             Test that the rating count and average changes on a delete.
         */
    }

    @Test
    @Disabled("Stub test")
    void validateTvKeywords()
    {
        /*
             TODO: Keywords and their Ids should be verified
         */
    }
}
